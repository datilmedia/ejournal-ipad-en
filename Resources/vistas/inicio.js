ventanaDeInicio = Titanium.UI.currentWindow;

Titanium.UI.setBackgroundColor('#000');

ventanaDeInicio.orientationModes = [ 
    Titanium.UI.PORTRAIT
];

var PageFlip = require('ti.pageflip');
var parse = require('parselogin');
var countrevistas= 0;
var countscrollview= 0;
var arregloDeImagenes = [];
var archivoLocal;
var detalle;
var limite = 100;
var salto = 0;
var titulo;
var articulos;
var i = 0;
var paginaActualDeParse = 1;
var sonido;
var banderaSonido = false;

var cargandoIngreso = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});
 
var indicadorDeIngreso = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font: {
    	font: 'Arial',
		fontSize: 24
	},
    color: '#FFF'
});
 
cargandoIngreso.add(indicadorDeIngreso);
			
var ventanaArchivo = Titanium.UI.createWindow({
	url: 'archivo.js',
	title: 'inicio',
	backgroundColor: 'white',
	width: '100%',
	height: '100%',
});

var vistaBotones = Ti.UI.createView({
	borderColor: '#7c848d',
	borderWidth: 0.5,
	borderRadius: 1,
	backgroundColor: 'black',
	width: '100%',
	height: 60,
	top: '0',
	left: 0,
});

var vistaBotonesLista = Ti.UI.createView({
	layout: 'vertical',
	width: '30%',
});

var vistaTextoLista = Ti.UI.createView({
	layout: 'vertical',
	width: '70%'
})
		
var vistaLista = Ti.UI.createView({
	backgroundColor: 'white',
	layout: 'horizontal',
	borderColor: '#666',
	borderWidth: 3,
	borderRadius: 0,
	width: '100%',
	height: '20%',
	top: '80%'
});
		
var vistaImagen = Ti.UI.createView({
	backgroundImage: '../fondo_webview.jpg',
	borderColor: '#000',
	borderWidth: 1,
	borderRadius: 1,
	backgroundColor: 'gray',
	//width: '100%',
	height: '80%',
	top: 60,
	left: 0
});

var buttonEntrar = Titanium.UI.createButton({
	backgroundImage: '../boton_azul.png',
	top: '46%',
	width: 300,
	height: 40
});

var botonSalir = Titanium.UI.createButton({
	title: 'Salir',
	backgroundImage: '../boton_gris.png',
	height: 40,
	width: 200,
	top: 10,
	left: 8
});

var botonEspanol = Titanium.UI.createButton({
	title: 'Español',
	backgroundImage:'../boton_azul.png',
	height: 40,
	width: 100,
	top: 10,
	right: '2%'
});

var botonEnglish = Titanium.UI.createButton({
	title: 'English',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 100,
	top: 10,
	right: '15.8%'			
});

var botonArchivo = Titanium.UI.createButton({
	title: 'Archivo',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 100,
	top: 10,
	right: '5%'
});

var botonAccion = Titanium.UI.createButton({
	title: 'Leer',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 200,
	top: '20'
});

var botonAudio = Titanium.UI.createButton({
	title: 'Escuchar',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 200,
	top: '20'
});

var botonEliminar = Titanium.UI.createButton({
	title: 'Eliminar',
	backgroundImage: '../boton_azul.png',
	height: 40,
	width: 200,
	top: '20'
});

var botonSiguiente = Titanium.UI.createButton({
	backgroundImage: '../boton_siguiente.png',
	height: 40,
	width: 40,
	top: 400,
	right: 20
});

var botonAnterior = Titanium.UI.createButton({
	backgroundImage: '../boton_anterior.png',
	height: 40,
	width: 40,
	top: 400,
	left: 20
});

var botonRegresar = Titanium.UI.createButton({
	title: 'Regresar',
	backgroundImage: '../boton_azul.png',
	height: 40,
	left: 10,
	width: 200,
	top: 10
});

var cargandoDatos = Ti.UI.createView({
    backgroundColor: 'black',
    opacity: 0.75,
    height: '100%',
    width: '100%'
});
 
var indicadorDeCargaDeDatos = Ti.UI.createActivityIndicator({
    style: Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
    font: {
    	font: 'Arial',
		fontSize : 24
	},
    color: '#FFF'
});
										

	botonSalir.title = 'Logout';
	botonAccion.title = 'Read';
	botonAudio.title = 'Listen';
	botonArchivo.title = 'Archive';
	botonRegresar.title = 'Back';
	botonEliminar.title = 'Delete';
	indicadorDeCargaDeDatos.message = 'Loading data, please wait...';
	botonEnglish.enabled = false;
	botonEspanol.enabled = true;

var textoTitulo = Ti.UI.createLabel({
	width: '90%',
	top: '20',
	left: '20',
	text: detalle,
	font: {
		fontSize: 20,
	}
});

var textoDetalle = Ti.UI.createLabel({
	width: '90%',
	left: '20',
	top: '20',
	text: detalle,
	font: {
		fontSize : 14
	},
	verticalAlign: Ti.UI.TEXT_VERTICAL_ALIGNMENT_TOP
});
		
var scrollView = Titanium.UI.createScrollableView({
	showPagingControl: true,
	pagingControlColor: '#142949',
	pagingControlHeight: 0,
	maxZoomScale: 2.0,
	currentPage: 0,
	bottom: '7.5%',
	height: '93%',
	width: '100%',
});
		
vistaBotones.setBackgroundGradient( {
	type: 'linear',
	//colors: ['#0f3865','#10427a'],
	colors: ['#142949', '#142949'],
	backFillStart: false
});
var banderapaginado = false;
	
botonSalir.addEventListener('click', function() {
	ventanaDeInicio.close({view:ventanaDeInicio,opacity:0,duration:500});
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}
});
	
botonEnglish.addEventListener('click', function() {
	Ti.App.Properties.setString('lenguaje', 'en');
	buttonEntrar.title = 'Login';
	botonSalir.title = 'Logout';
	botonArchivo.title = 'Archive';
	botonEliminar.title = 'Delete';
	botonRegresar.title = 'Back';
	botonAccion.title = 'Read';
	botonAudio.title = 'Listen';
	botonEnglish.enabled = false;
	botonEspanol.enabled = true;
	salto = 0;
	i = 0;
	botonSiguiente.hide();
	botonAnterior.hide();
	cargaArticulos(limite, salto);
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}
});
		
botonEspanol.addEventListener('click', function() {
	Ti.App.Properties.setString('lenguaje', 'es');
	buttonEntrar.title = 'Ingresar';
	botonSalir.title = 'Salir';
	botonArchivo.title = 'Archivo';
	botonEliminar.title = 'Eliminar';
	botonRegresar.title = 'Regresar';
	botonAccion.title = 'Leer';
	botonAudio.title = 'Escuchar';
	botonEnglish.enabled = true;
	botonEspanol.enabled = false;
	salto = 0;
	i = 0;
	botonSiguiente.hide();
	botonAnterior.hide();
	cargaArticulos(limite, salto);
	if (banderaSonido) { 
		sonido.stop();
		banderaSonido = false;
	}
});

botonArchivo.addEventListener('click', function() {
	ventanaArchivo.open({transition:Ti.UI.iPhone.AnimationStyle.CURL_UP});
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}
});

scrollView.addEventListener('scroll', function(e) {
	i = e.currentPage;
	
	try {
		textoTitulo.text = articulos[i].title;
		textoDetalle.text = articulos[i].description;
		archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].file.url.substr(articulos[i].file.url.length -11, articulos[i].file.url.length -1))
		if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').exists()) {
			botonEliminar.enabled = true;
		} else {
			botonEliminar.enabled = false;
		}
	} catch (e) {
		
	}

	if (i == articulos.length -1 && paginaActualDeParse < totalPaginas ) {
		botonSiguiente.show();
	} else {
		botonSiguiente.hide();
	}
	
	if (i == 0 && paginaActualDeParse  > 1 ) {
		botonAnterior.show();
	} else {
		botonAnterior.hide();
	}		

	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}

});

botonAnterior.addEventListener('click', function(e) {
	paginaActualDeParse = paginaActualDeParse - 1;
	
	if (i == articulos.length-1 && paginaActualDeParse + 1 < totalPaginas ) {
		botonSiguiente.show();
	} else {
		botonSiguiente.hide();
	}

	if (i == 0 && paginaActualDeParse -1 > 1) {
		botonAnterior.show();	
	} else {
		botonAnterior.hide();
	}
			
	cargaArticulos(limite, salto - limite);
	
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}
});

botonSiguiente.addEventListener('click', function(e) {
	paginaActualDeParse = paginaActualDeParse + 1;

	if (i == articulos.length-1 && paginaActualDeParse + 1 < totalPaginas ) {
		botonSiguiente.show();
	} else {
		botonSiguiente.hide();
	}

	if (paginaActualDeParse-1 > 1  && i == 0) {
		botonAnterior.show();	
	} else {
		botonAnterior.hide();
	}
	
	cargaArticulos(limite, salto + limite); 
	
	if (banderaSonido) {
		sonido.stop();
		banderaSonido = false;
	}
});
/*
botonAudio.addEventListener('click', function()
{
	sound.play();
	pb.max = sound.duration;
});
*/

botonSiguiente.hide();
botonAnterior.hide();
scrollView.add(botonSiguiente);
scrollView.add(botonAnterior);

vistaBotones.add(botonSalir);
vistaBotones.add(botonArchivo);

vistaImagen.add(scrollView);

vistaBotonesLista.add(botonAccion);
vistaBotonesLista.add(botonEliminar);
//vistaBotonesLista.add(botonAudio);

vistaTextoLista.add(textoTitulo);
vistaTextoLista.add(textoDetalle);

vistaLista.add(vistaTextoLista);
vistaLista.add(vistaBotonesLista);

ventanaDeInicio.add(vistaBotones);
ventanaDeInicio.add(vistaImagen);
ventanaDeInicio.add(vistaLista);

botonAccion.addEventListener('click', function() {	
	Ti.API.info('Estamos en la accion de leer... ' + articulos[i].file.url.substr(articulos[i].file.url.length -11, articulos[i].file.url.length -1));
	var nombreArticulo = articulos[i].screenname+'.pdf';
	var descargarArticulo = articulos[i].file.url;
	titulo = articulos[i].title;

	if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').exists() && (botonAccion.title == "Leer" || botonAccion.title == "Read") ) {
		var pdfFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nombreArticulo);
		
		Titanium.UI.setBackgroundColor('#000');
		var scrollViewPdf = Ti.UI.createScrollView({
		    zoomScale: 1.0,
		    minZoomScale: 1.0,
		    maxZoomScale: 4.0,
		    top: 60
		});

		var pageflip = PageFlip.createView({
	        /* All Options: TRANSITION_FLIP [default], TRANSITION_SLIDE, TRANSITION_FADE, TRANSITION_CURL */
	        transition: PageFlip.TRANSITION_CURL,
	        transitionDuration: 1,
	        pdf: pdfFile.nativePath,
	        tapToAdvanceWidth: '15%',
	        landscapeShowsTwoPages: true
	    });
									
		scrollViewPdf.add(pageflip);	
		
		scrollViewPdf.addEventListener('doubletap', function() {
	        scrollViewPdf.zoomScale = 1.0;
	    });
	    
	     pageflip.addEventListener('change', function(evt) {
	        scrollViewPdf.zoomScale = 1.0;	       
	    });
		
		var ventanaDeLectura = Titanium.UI.createWindow({
			backgroundColor: 'white',
			width: '100%',
			height: '100%'
		});
		
		var vistaBotonesLectura = Ti.UI.createView({
			borderColor: '#000',
			borderWidth: 1,
			borderRadius: 1,
			backgroundColor: 'white',
			width: '100%',
			height: 60,
			top: '0',
			left: 0
		});

		vistaBotonesLectura.add(botonRegresar);

		botonRegresar.addEventListener('click', function() {
			ventanaDeLectura.close({
				view: ventanaDeInicio,
				opacity: 0,
				duration: 500
			});
		});
		
		ventanaDeLectura.add(vistaBotonesLectura);
		ventanaDeLectura.add(scrollViewPdf);
		ventanaDeLectura.open({transition:Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT});

	} else {
		botonAccion.enabled = false;


			botonAccion.title = "Downloading...";
		
		
		var xhr = Titanium.Network.createHTTPClient();	
		
		xhr.onload = function() {
			var ruta = this.location.substr(this.location.length -11, this.location.length-1);
			var nombrearchivo = articulos[i].screenname+'.pdf';
			var fecha = articulos[i].screenname;
			var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nombrearchivo);
			f.write(this.responseData);
			archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombrearchivo)
			
			if (archivoLocal.exists()) {	
				botonAccion.enabled = true;
				

					botonAccion.title = "Read";
				
				//nombrearchivo = Titanium.Filesystem.applicationDataDirectory + ruta;

				var db = Titanium.Database.open('spurrierdb');
				db.execute("CREATE TABLE IF NOT EXISTS SPURRIER (TITULO TEXT, NOMBRE_ARCHIVO TEXT, FECHA TEXT)");
				db.execute("INSERT INTO SPURRIER (TITULO, NOMBRE_ARCHIVO, FECHA ) VALUES(?,?,?)", titulo, nombrearchivo, fecha);	
				db.close();
				botonEliminar.enabled = true;
	
			} else {
				botonAccion.title = "Downloading...";
			}

		};
		xhr.open('GET', articulos[i].file.url);
		xhr.send();
	}
});		

botonEliminar.addEventListener('click', function(){
	if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').exists()) {
		var nombrearchivo = articulos[i].screenname+'.pdf';
		Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, articulos[i].screenname+'.pdf').deleteFile();
		botonEliminar.enabled = false;
	}
});

botonAudio.addEventListener('click', function() {
	var nombreAudio = articulos[i].audio.name;
	var titulo = articulos[i].title;
	
	if (Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombreAudio).exists() && (botonAudio.title == "Escuchar" || botonAudio.title == "Listen") ) {
		var audioFile = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nombreAudio);
		sonido = Titanium.Media.createSound({
			url: audioFile
		});
		sonido.play();
		banderaSonido = true;
	} else {
		botonAudio.enabled = false;


			botonAudio.title = "Downloading...";
	
		
		var xhr = Titanium.Network.createHTTPClient();	
		
		xhr.onload = function() {
			var ruta = articulos[i].audio.name;
			Ti.API.info(ruta);
			var fecha = articulos[i].screenname;
			var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, ruta);
			f.write(this.responseData);
			Ti.API.info(f);
			archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, ruta)
			
			if (archivoLocal.exists()) {
				Ti.API.info('existe el archivo de audio local')	
				botonAudio.enabled = true;
				
				if (Ti.App.Properties.getString('lenguaje') == 'es'){
					botonAudio.title = "Escuchar";
					
				} else {
					botonAudio.title = "Listen";
				
				};
				
				nombreAudio = Titanium.Filesystem.applicationDataDirectory + ruta;

				var db = Titanium.Database.open('spurrierdbaudio');
				db.execute("CREATE TABLE IF NOT EXISTS SPURRIERAUDIO (NOMBRE_AUDIO TEXT, FECHA TEXT)");
				db.execute("INSERT INTO SPURRIERAUDIO (NOMBRE_AUDIO, FECHA) VALUES(?,?)", nombreAudio, fecha);	
				db.close(); 
			} else {
				botonAccion.title = "Descargando...";
			}

		};
		xhr.open('GET', articulos[i].audio.url);
		xhr.send();
	}
	
});

function cargaArticulos(arg1, arg2) {
	i = 0;
	salto = arg2;
	ventanaDeInicio.add(cargandoDatos);
	indicadorDeCargaDeDatos.show();
	
	if (scrollView.views) {
		arregloDeImagenes = [];
	}
		
	var parse = require('parse');
	var client = new parse.Client();
	var whereClause = {
		"ln": "en"
	};
	
	client.get({
		className : 'Editions',
		payload : {
			"where": JSON.stringify(whereClause),
			"count": 1,
			"order": "-edition",
			"limit": limite,
			"skip": salto
		},
		success : function(response) {
			try {
				articulos= 0;
				articulos = response.results;
				totalPaginas = Math.ceil(response.count/limite);

				countrevistas= articulos.length;
				banderita = true;	
				countscrollview = 0;	
		
				for (var c = 0; c <response.results.length; c++) {							
				
					var nombrearchivo = response.results[c].screenshot.url.substr(response.results[c].screenshot.url.length -11, response.results[c].screenshot.url.length -1);
					var archivoimagen= Titanium.Filesystem.applicationDataDirectory + nombrearchivo ;
					var existeArchivo = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombrearchivo);
					
					if (existeArchivo.exists()==0) {
						banderita = false;
						
						nombrearchivo = response.results[c].screenname;
						Ti.API.info(nombrearchivo);
						var xhr = Titanium.Network.createHTTPClient();
						xhr.onload = function (){
							var ruta = this.location.substr(this.location.length -11, this.location.length-1);
							var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, ruta );
							f.write(this.responseData);
							
							countscrollview ++;
							
							if ( countrevistas == countscrollview) {
									scrollView.views = arregloDeImagenes;
									ventanaDeInicio.remove(cargandoDatos);	
							}
				
						};
						
						xhr.open('GET', response.results[c].screenshot.url);
						xhr.send();
						var m = Ti.UI.create2DMatrix({ scale: 0.80 });
						var av = Ti.UI.createImageView({
							image : archivoimagen
						});
						av.setTransform(m);
						
						scrollView.currentPage = 0;		
						textoDetalle.text = articulos[scrollView.currentPage].description;
						textoTitulo.text = articulos[scrollView.currentPage].title;
						
						botonAnterior.show();
						if (scrollView.currentPage == articulos.length-1 && paginaActualDeParse < totalPaginas ) {
							botonSiguiente.show();
							
						} else {
							botonSiguiente.hide();
						};
						if (scrollView.currentPage == 0 && paginaActualDeParse  > 1 ) {
							botonAnterior.show();
							
						} else {
							botonAnterior.hide();
						};	
						
						arregloDeImagenes[c] = av;	
				
					} else {
						scrollView.currentPage = 0;		
						textoDetalle.text = articulos[scrollView.currentPage].description;
						textoTitulo.text = articulos[scrollView.currentPage].title;
				
						if (scrollView.currentPage == articulos.length-1 && paginaActualDeParse < totalPaginas ) {
							botonSiguiente.show();
							
						} else {
							botonSiguiente.hide();
						};
						if(scrollView.currentPage == 0 && paginaActualDeParse  > 1 ){
							botonAnterior.show();
							
						} else {
							botonAnterior.hide();
						};		
						var m = Ti.UI.create2DMatrix({ scale: 0.80 });				
						var av = Ti.UI.createImageView({
							image: archivoimagen
						});
						av.setTransform(m);
											
						arregloDeImagenes[c] = av;
						countscrollview ++;
						if ( countrevistas == countscrollview) {
								scrollView.views = arregloDeImagenes;
								ventanaDeInicio.remove(cargandoDatos);	
						}
					};			
				}
				scrollView.currentPage = 0;	
				Ti.API.info(nombrearchivo);								
				archivoLocal = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, nombrearchivo);
		
			} catch(E) {
				
			}		
		},
		error: function(response, xhr) {
			
		}
	});
}
cargaArticulos(limite, salto);
